import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import rootData from '../reducers'

const store = createStore(
    rootData,
    applyMiddleware(thunk)
);


export default store;
