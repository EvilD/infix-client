import {
    AUTH_SUCCESS,
    AUTH_FAILURE,
    REGISTER_SUCCESS,
    REGISTER_FAILURE,
    CAR_SUCCESS,
    CAR_FAILURE,
    ADD_REPAIR_FAILURE,
    ADD_REPAIR_SUCCESS,
    EDIT_USER_FAILURE,
    EDIT_USER_SUCCESS,
    UPDATE_PRICING_FAILURE,
    UPDATE_PRICING_SUCCESS,
    ADD_PRICING_SUCCESS,
    ADD_PRICING_FAILURE,
    ADD_RESERVATION_SUCCESS,
    ADD_RESERVATION_FAILURE,
    UPDATE_RESERVATION_SUCCESS,
    UPDATE_RESERVATION_FAILURE,
    UPDATE_REPAIR_SUCCESS,
    UPDATE_REPAIR_FAILURE
} from "../actions";

const initialState = {};

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case AUTH_SUCCESS:
            return {
                ...state,
                idUser: action.payload.data[0].iduser,
                login: action.payload.data[0].login,
                roleID: action.payload.data[0].permision,
            };
        case AUTH_FAILURE:
            return {
                errID: action.err,
            };
        case REGISTER_SUCCESS:
            return {
                ...state,
                registerStatus: action.payload.data,
            };
        case REGISTER_FAILURE:
            return {
                errID: action.err,
            };
        case CAR_SUCCESS:
            return {
                ...state,
                statusCarID: action.type,
            };
        case CAR_FAILURE:
            return {
                statusCarID: action.type,
                errID: action.err,
            };
        case ADD_REPAIR_SUCCESS:
            return {
                ...state,
                statusRepairID: action.type,
            };
        case ADD_REPAIR_FAILURE:
            return {
                statusRepairID: action.type,
                errID: action.err,
            };
        case EDIT_USER_SUCCESS:
            return {
                ...state,
                statusEditID: action.type,
            };
        case EDIT_USER_FAILURE:
            return {
                statusEditID: action.type,
                errID: action.err,
            };
        case UPDATE_PRICING_SUCCESS:
            return {
                ...state,
                statusPricingID: action.type,
            };
        case UPDATE_PRICING_FAILURE:
            return {
                statusPricingID: action.type,
                errID: action.err,
            };
        case ADD_PRICING_SUCCESS:
            return {
                ...state,
                statusAddPricingID: action.type,
            };
        case ADD_PRICING_FAILURE:
            return {
                statusAddPricingID: action.type,
                errID: action.err,
            };
        case ADD_RESERVATION_SUCCESS:
            return {
                ...state,
                statusAddReservationID: action.type,
            };
        case ADD_RESERVATION_FAILURE:
            return {
                statusAddReservationID: action.type,
                errID: action.err,
            };
        case UPDATE_RESERVATION_SUCCESS:
            return {
                ...state,
                statusAddReservationID: action.type,
            };
        case UPDATE_RESERVATION_FAILURE:
            return {
                statusAddReservationID: action.type,
                errID: action.err,
            };
        case UPDATE_REPAIR_SUCCESS:
            return {
                ...state,
                statusAddReservationID: action.type,
            };
        case UPDATE_REPAIR_FAILURE:
            return {
                statusAddReservationID: action.type,
                errID: action.err,
            };
        default:
            return state;
    }
}

export default rootReducer;