export const theme = {
    primary: '#dc0d15',
    primaryDarker: '#96090e',
    accept: '#28a745',
    acceptDarker: '#217c3e',
    secondary: '#666666',
    secondaryDarker: '#404040',
    lite: 300,
    bold: 600,
};
