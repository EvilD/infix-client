import {createGlobalStyle} from 'styled-components';
import 'bootstrap/dist/css/bootstrap.min.css';


const GlobalStyle = createGlobalStyle`
      @import url('https://fonts.googleapis.com/css2?family=Manrope:wght@300;600&display=swap');

    *, *, html::before, *:after{
        margin: 0;
        padding: 0;
        scroll-behavior: smooth;
        box-sizing: border-box;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;

        font-family: 'Manrope', sans-serif;
    }
`;

export default GlobalStyle;