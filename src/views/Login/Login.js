import React from 'react';
import axios from 'axios';
import styled, {ThemeProvider} from "styled-components";
import GlobalStyle from "theme/globalStyle";
import {theme} from "theme/mainTheme"
import {connect, Provider} from 'react-redux'
import store from 'store'
import {Redirect} from 'react-router-dom'
import {Field, Form, Formik} from "formik";
import Input from "../../components/atoms/Input/Input";
import Button from "../../components/atoms/Button/Button";
import {AUTH_FAILURE, AUTH_REQUEST, AUTH_SUCCESS, authenticate as authenticateAction} from "../../actions";
import {Card, Container, Row, Col, Alert} from 'react-bootstrap'

const Wrapper = styled.div`
  background-color: ${({theme}) => theme.secondaryDarker};
  height: 100vh;
`;


const Login = ({roleID, idUser, login, errID, authenticate}) => {
    let alert;
    let email;
    let phoneNumber;

    if(roleID === 0){
        roleID = '0'
    }


    if (roleID) {
        axios.get('http://localhost:8080/edit/' + login)
            .then(response => {
                email = response.data[0].email;
                phoneNumber = response.data[0].telephoneNumber;

                sessionStorage.setItem('userData', JSON.stringify({
                    'user': login,
                    'role': roleID.toString(),
                    'id': idUser.toString(),
                    'email': email,
                    'phoneNumber': phoneNumber
                }));

                document.location.href="/";
            });
    }

    if (errID) {
        alert = <Alert variant="danger">Niepoprawne dane logowania!</Alert>
    }

    return (
        <>
        <Wrapper>
            <Provider store={store}>
                <ThemeProvider theme={theme}>
                    <>
                        <Container>
                            <Row className="justify-content-md-center pt-5">
                                <Col lg="6">
                                    <Card>
                                        <Card.Title className="text-center m-3"><h1>Logowanie</h1></Card.Title>
                                        <Card.Body>
                                            {alert}
                                            <Formik
                                                initialValues={{login: "", password: ""}}
                                                onSubmit = {({login, password}) => {
                                                    authenticate(login, password)
                                                }}
                                                render={() => (
                                                    <Form>
                                                        <Field
                                                            name="login"
                                                            render={({field, form, meta}) => (
                                                                <div>
                                                                    <Input className="w-100 m-2" {...field}
                                                                           placeholder="Login" type="text"/>
                                                                </div>
                                                            )}
                                                        />
                                                        <Field
                                                            name="password"
                                                            render={({field, form, meta}) => (
                                                                <div>
                                                                    <Input className="w-100 m-2" {...field}
                                                                           placeholder="Hasło" type="password"/>
                                                                </div>
                                                            )}
                                                        />
                                                        <hr/>
                                                        <Button type="submit" width={"100%"}>Zaloguj</Button>
                                                    </Form>
                                                )}
                                            />
                                        </Card.Body>
                                    </Card>
                                </Col>
                            </Row>
                        </Container>
                    </>
                </ThemeProvider>

            </Provider>
        </Wrapper>
            <GlobalStyle/>
            </>
    )
};

const mapStateToProps = ({idUser = null, roleID = null, login = null, errID = null}) => ({
    idUser,
    roleID,
    login,
    errID
});

const mapDispatchToProps = dispatch => ({
    authenticate: (login, password) =>
        dispatch(authenticateAction(login, password))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
