import React from 'react';
import {ThemeProvider} from "styled-components";
import GlobalStyle from "theme/globalStyle";
import {theme} from "theme/mainTheme";
import {Provider} from 'react-redux'
import store from 'store'
import {Route, Switch, BrowserRouter, Redirect} from 'react-router-dom'

import Login from "../Login/Login";
import Register from "../Register/Register";
import ControlPanel from "../ControlPanel/ControlPanel";
import MainPage from "../MainPage/MainPage";
import StatusPage from "../StatusPage/StatusPage";


const Root = () => {

    const NotFoundRedirect = () => <Redirect to='/'/>

    return (
        <>
            <Provider store={store}>
                <ThemeProvider theme={theme}>
                    <>
                        <BrowserRouter>
                            <Switch>
                                <Route exact path="/" component={MainPage}/>
                                <Route path="/login/" component={Login}/>
                                <Route path="/register/" component={Register}/>
                                <Route path="/controlPanel/" component={ControlPanel}/>
                                <Route path="/status/:id/" component={StatusPage}/>
                                <Route component={NotFoundRedirect}/>
                            </Switch>
                        </BrowserRouter>
                    </>
                </ThemeProvider>
            </Provider>
            <GlobalStyle/>
        </>
    )
};

export default Root;
