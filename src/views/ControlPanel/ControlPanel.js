import React, {useState} from 'react';
import styled, {ThemeProvider} from "styled-components";
import GlobalStyle from "theme/globalStyle";
import {theme} from "theme/mainTheme"
import {Provider} from 'react-redux'
import store from 'store'
import {Redirect} from "react-router-dom";

import NavBar from "components/molecules/NavBar/NavBar";
import Tabs from "components/molecules/Tabs/Tabs";

const ControlPanel = () => {

    const userData = JSON.parse(sessionStorage.getItem('userData'))
    if (!userData) {
        return <Redirect to={"/"}/>
    }

    return (
        <>
            <Provider store={store}>
                <ThemeProvider theme={theme}>
                    <>
                        <NavBar/>
                        <Tabs/>
                    </>
                </ThemeProvider>
            </Provider>
            <GlobalStyle/>
        </>
    )
};


export default ControlPanel;
