import React, {useState} from 'react';
import styled, {ThemeProvider} from "styled-components";
import GlobalStyle from "theme/globalStyle";
import {theme} from "theme/mainTheme"
import {connect, Provider} from 'react-redux'
import store from 'store'
import {Redirect} from 'react-router-dom'
import {Field, Form, Formik} from "formik";
import Input from "../../components/atoms/Input/Input";
import Button from "../../components/atoms/Button/Button";
import {register as registerAction} from "../../actions";
import {Card, Container, Row, Col, Alert} from 'react-bootstrap'

const Wrapper = styled.div`
  background-color: ${({theme}) => theme.secondaryDarker};
  height: 100vh;
`;


const Register = ({errID, registerStatus, register}) => {
    let alert;

    if (registerStatus) {
        return <Redirect to={"/"}/>
    }

    if (errID) {
        alert = <Alert variant="danger">Niepoprawne dane rejestracji!</Alert>
    }

    return (
        <>
            <Wrapper>
                <Provider store={store}>
                    <ThemeProvider theme={theme}>
                        <>
                            <Container>
                                <Row className="justify-content-md-center pt-5">
                                    <Col lg="6">
                                        <Card>
                                            <Card.Title className="text-center m-3"><h1>Rejestracja</h1></Card.Title>
                                            <Card.Body>

                                                {alert}

                                                <Formik
                                                    initialValues={{
                                                        name: "",
                                                        surname: "",
                                                        pesel: "",
                                                        driversLicense: "",
                                                        password: "",
                                                        login: "",
                                                        email: "",
                                                        telephoneNumber: ""
                                                    }}
                                                    onSubmit={({name, surname, pesel, driversLicense, password, login, email, telephoneNumber}) => {
                                                        register(name, surname, pesel, driversLicense, password, login, email, telephoneNumber)
                                                    }}
                                                    render={() => (
                                                        <Form>
                                                            <Field
                                                                name="name"
                                                                render={({field, form, meta}) => (
                                                                    <div>
                                                                        <Input className="w-100 m-2" {...field}
                                                                               placeholder="Imię" type="text"/>
                                                                    </div>
                                                                )}
                                                            />
                                                            <Field
                                                                name="surname"
                                                                render={({field, form, meta}) => (
                                                                    <div>
                                                                        <Input className="w-100 m-2" {...field}
                                                                               placeholder="Nazwisko" type="text"/>
                                                                    </div>
                                                                )}
                                                            />
                                                            <Field
                                                                name="driversLicense"
                                                                render={({field, form, meta}) => (
                                                                    <div>
                                                                        <Input className="w-100 m-2" {...field}
                                                                               placeholder="Numer prawa jazdy"
                                                                               type="text"/>
                                                                    </div>
                                                                )}
                                                            />
                                                            <Field
                                                                name="pesel"
                                                                render={({field, form, meta}) => (
                                                                    <div>
                                                                        <Input className="w-100 m-2" {...field}
                                                                               placeholder="PESEL" type="text"/>
                                                                    </div>
                                                                )}
                                                            />
                                                            <Field
                                                                name="login"
                                                                render={({field, form, meta}) => (
                                                                    <div>
                                                                        <Input className="w-100 m-2" {...field}
                                                                               placeholder="Login" type="text"/>
                                                                    </div>
                                                                )}
                                                            />
                                                            <Field
                                                                name="password"
                                                                render={({field, form, meta}) => (
                                                                    <div>
                                                                        <Input className="w-100 m-2" {...field}
                                                                               placeholder="Hasło" type="password"/>
                                                                    </div>
                                                                )}
                                                            />
                                                            <Field
                                                                name="telephoneNumber"
                                                                render={({field, form, meta}) => (
                                                                    <div>
                                                                        <Input className="w-100 m-2" {...field}
                                                                               placeholder="Numer telefonu"
                                                                               type="number"/>
                                                                    </div>
                                                                )}
                                                            />
                                                            <Field
                                                                name="email"
                                                                render={({field, form, meta}) => (
                                                                    <div>
                                                                        <Input className="w-100 m-2" {...field}
                                                                               placeholder="E-mail" type="text"/>
                                                                    </div>
                                                                )}
                                                            />
                                                            <hr/>
                                                            <Button type="submit" width={"100%"}>Zarejestruj</Button>
                                                        </Form>
                                                    )}
                                                />
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                </Row>
                            </Container>
                        </>
                    </ThemeProvider>
                </Provider>
            </Wrapper>
            <GlobalStyle/>
        </>
    )
};

const mapStateToProps = ({registerStatus = null, errID = null}) => ({
    registerStatus,
    errID
});

const mapDispatchToProps = dispatch => ({
    register: (name, surname, pesel, driversLicense, password, login, email, telephoneNumber) =>
        dispatch(registerAction(name, surname, pesel, driversLicense, password, login, email, telephoneNumber))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);
