import React, {useState} from 'react';
import {ThemeProvider} from "styled-components";
import GlobalStyle from "theme/globalStyle";
import {theme} from "theme/mainTheme"
import {Provider} from 'react-redux'
import store from 'store'

import NavBar from "components/molecules/NavBar/NavBar";
import StatusItem from "../../components/atoms/StatusItem/StatusItem";
import {Badge, Container} from "react-bootstrap";
import {Redirect} from "react-router-dom";

const StatusPage = (props) => {

    const userData = JSON.parse(sessionStorage.getItem('userData'))

    var id_search = props.match.params.id

    if(!id_search){
        return <Redirect to={"/"}/>
    }

    const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    const listItems = numbers.map((number) =>
        <StatusItem id={number} comment={"asdasdasdasdasd"} date={"20.05.2020 21:37"}/>
    );

    return (
        <>
            <Provider store={store}>
                <ThemeProvider theme={theme}>
                    <NavBar/>
                    <Container>
                        <div className={"mt-3"}>
                            <h3 className={"text-center"}>Numer naprawy: {id_search}</h3>
                            {/*<h1 className="text-center"><Badge className="mr-1"*/}
                            {/*                                   variant={!status ? "success" : "secondary"}>{!status ? "W realizacji" : "Zakończone"}</Badge>*/}
                            {/*</h1>*/}
                        </div>
                        {listItems}
                    </Container>
                </ThemeProvider>
            </Provider>
            <GlobalStyle/>
        </>

    )
};


export default StatusPage;
