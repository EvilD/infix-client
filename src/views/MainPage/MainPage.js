import React from 'react'

import NavBar from "components/molecules/NavBar/NavBar";
import Menu from "components/molecules/Menu/Menu";
import Jumbotron from "components/molecules/Jumbotron/Jumbotron";
import Offer from "components/molecules/Offer/Offer";
import Location from "components/molecules/Location/Location";
import Footer from "../../components/molecules/Footer/Footer";
import ReservationForm from "../../components/molecules/ReservationForm/ReservationForm";
import ValuationForm from "../../components/molecules/ValuationForm/ValuationForm";


const MainPage = () => (
    <>
        <NavBar/>
        {window.location.pathname == '/' ? <Menu/> : ''}
        <Jumbotron search/>
        <Offer/>
        <ReservationForm/>
        <Jumbotron/>
        <ValuationForm/>
        <Location/>
        <Footer/>
    </>
);

export default MainPage;
