import React, {useState} from "react";
import {Badge, Card, Col, Container, Modal, Row} from "react-bootstrap";
import Button from "../Button/Button";
import Input from "../Input/Input";
import {Field, Form, Formik} from "formik";
import {editUser as editUserAction} from "../../../actions";
import {connect} from "react-redux";

const UserItem = ({id, login, email, phone, role, editUser}) => {

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    var variant;
    var monit;

    switch (role) {
        case '0':
            variant = "success"
            monit = "Użytkownik"
            break;
        case '1':
            variant = "warning"
            monit = "Pracownik"
            break;
        case '2':
            variant = "danger"
            monit = "Administrator"
            break;
    }

    return (
        <>
            <Card className="m-3">
                <Card.Body>
                    <Card.Title>
                        <Container fluid>
                            <Row>
                                <Col xs={12} md={7}>
                                    <h4 className="d-inline-block"><Badge className="mr-1"
                                                                          variant={variant}>{monit}</Badge>
                                    </h4>
                                    <b className="d-inline-block">Id użytkownika: {id}</b><br/>
                                    <p className="d-inline-block">Login: {login}</p><br/>
                                    <p className="d-inline-block">Email: {email}</p><br/>
                                    <p className="d-inline-block">Numer telefonu: {phone}</p><br/>
                                </Col>
                                <Col xs={12} md={5}>
                                    <p className="d-block text-center">
                                        Edytuj użytkownika<br/>
                                        <Button onClick={handleShow} width={"100%"}>Edytuj</Button>
                                    </p>
                                </Col>
                            </Row>
                        </Container>
                    </Card.Title>
                </Card.Body>
            </Card>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Edycja użytkownika: {login}</Modal.Title>
                </Modal.Header>
                <Formik
                    initialValues={{login: login, email: email, telephoneNumber: phone, password: ""}}
                    onSubmit={({login, email, telephoneNumber, password}) => {
                        editUser(login, email, telephoneNumber, password)
                    }}
                    render={() => (
                        <>
                            <Form>
                                <Modal.Body>
                                    <h5 className="d-inline-block">Id użytkownika: {id}</h5><br/>
                                    <hr/>
                                    <Field
                                        name="login"
                                        render={({field, form, meta}) => (
                                            <div>
                                                <input className={"form-control mt-3"} {...field}
                                                       placeholder="Login" type="text" disabled/>
                                            </div>
                                        )}
                                    />
                                    <Field
                                        name="email"
                                        render={({field, form, meta}) => (
                                            <div>
                                                <input className={"form-control mt-3"} {...field}
                                                       placeholder="Adres email" type="text"/>
                                            </div>
                                        )}
                                    />
                                    <Field
                                        name="telephoneNumber"
                                        render={({field, form, meta}) => (
                                            <div>
                                                <input className={"form-control mt-3"} {...field}
                                                       placeholder="Numer Telefonu" type="number"/>
                                            </div>
                                        )}
                                    />
                                    <Field
                                        name="password"
                                        render={({field, form, meta}) => (
                                            <div>
                                                <input className={"form-control mt-3"} {...field}
                                                       placeholder="Hasło" type="password"/>
                                            </div>
                                        )}
                                    />
                                    {/*<Field as="select" name="role" className="form-control mt-3">*/}
                                    {/*    <option value="0">Użytkownik</option>*/}
                                    {/*    <option value="1">Pracownik</option>*/}
                                    {/*    <option value="2">Administrator</option>*/}
                                    {/*</Field>*/}
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="primary" type="submit">
                                        Zapisz
                                    </Button>
                                    <Button secondary onClick={handleClose}>
                                        Zamknij
                                    </Button>
                                </Modal.Footer>
                            </Form>
                        </>
                    )}
                />
            </Modal>
        </>
    )
};

const mapStateToProps = ({errID = null, statusEditID = null}) => ({
    statusEditID,
    errID
});

const mapDispatchToProps = dispatch => ({
    editUser: (login, email, telephoneNumber, password) =>
        dispatch(editUserAction(login, email, telephoneNumber, password))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserItem);