import React from 'react';
import { storiesOf } from '@storybook/react';
import Input from './Input';

storiesOf('Input', module)
    .add('Input', () => <Input placeholder={'Login'}></Input>)
    .add('Search', () => <Input search placeholder={'Numer usługi'}></Input>);