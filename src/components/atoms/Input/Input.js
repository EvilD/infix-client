import styled, {css} from 'styled-components';
import magnifierIcon from 'assets/magnifier.svg';

const Input = styled.input`
  padding: 15px 30px;
  height: 47px;
  width: ${({width}) => width || '250px'};
  font-size: 16px;
  background-color: #eee;
  border: 1px solid #ddd;
  border-radius: 50px;
  
  ::placeholder{
    text-transform: uppercase;
    letter-spacing: 1px;
    color: grey;
  }
  
  ${({search}) =>
    search &&
    css`
      padding: 10px 20px 10px 45px;
      width: 500px;
      background-image: url(${magnifierIcon});
      background-size: 20px;
      background-position: 15px 50%;
      background-repeat: no-repeat;
       @media (max-width: 768px) {
          width: 100%;
       }
      
    `}
    
`;

export default Input;
