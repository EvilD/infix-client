import React from "react";
import {Card, Badge, Container, Row, Col} from "react-bootstrap";
import Button from "../Button/Button";


const CarItem = ({name, surname, email, phone, user, car, model, vin, engineCapacity, yearOf}) => (
    <Card className="m-3">
        <Card.Body>
            <Card.Title>
                <Container fluid>
                    <Row>
                        <Col xs={12} md={7}>
                            <p className="d-inline-block">Samochód: {car + ' ' + model}</p><br/>
                            <p className="d-inline-block">Numer VIN: {vin}</p><br/>
                            <p className="d-inline-block">Pojemość silnika: {engineCapacity}</p><br/>
                            <p className="d-inline-block">Rok produkcji: {yearOf}</p><br/>

                        </Col>
                        <Col xs={12} md={5}>
                            {user == '2' &&
                            <>
                                <p className="d-inline-block">Imię Nazwisko: {name + ' ' + surname }</p><br/>
                                <p className="d-inline-block">Email: {email}</p><br/>
                                <p className="d-inline-block">Numer telefonu: {phone}</p><br/>
                            </>
                            }

                        </Col>
                    </Row>
                </Container>
            </Card.Title>
        </Card.Body>
    </Card>
);

export default CarItem;