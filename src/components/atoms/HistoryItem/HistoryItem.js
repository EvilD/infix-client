import React from "react";
import {Card, Badge, Container, Row, Col} from "react-bootstrap";

const HistoryItem = ({status, car, model, vin}) => {

    return (
        <Card className="m-3">
            <Card.Body>
                <Card.Title>
                    <Container fluid>
                        <Row>
                            <Col xs={12} md={7}>
                                <h4 className="d-inline-block"><Badge className="mr-1"
                                                                      variant={status !== 'ukonczono' ? "success" : "secondary"}>{status !== 'ukonczono'? "W realizacji" : "Zakończone"}</Badge>
                                </h4>
                                <br/>
                                <p className="d-inline-block">Samochód: {car + ' ' + model}</p><br/>
                                <p className="d-inline-block">Numer VIN: {vin}</p><br/>
                            </Col>
                            <Col xs={12} md={5}>
                                <div className="d-block">
                                    <p className="d-inline-block">Numer śledzenia: <b>{vin}</b></p><br/>
                                    <p className="d-inline-block">Status: {status}</p><br/>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </Card.Title>
            </Card.Body>
        </Card>
    )
};

export default HistoryItem;