import React from "react";
import {Card, Badge, Container, Row, Col, Alert} from "react-bootstrap";
import Button from "../Button/Button";
import Input from "../Input/Input";
import {Field, Form, Formik} from "formik";
import {updatePricing as updatePricingAction} from "../../../actions";
import {connect} from "react-redux";


const ValuationItem = ({id, comment, date, price, updatePricing}) => {

    const userData = JSON.parse(sessionStorage.getItem('userData'))


    return (
        <Card className="m-3">
            <Card.Body>
                <Card.Title>
                    <Container fluid>
                        <Row>
                            <Col>
                                <h4 className="d-inline-block"><Badge className="mr-1"
                                                                      variant={!price ? "success" : "secondary"}>{!price ? "W realizacji" : "Zakończone"}</Badge>
                                </h4>
                                <p className="d-inline-block">Numer zgłoszenia: {id}</p><br/>
                                <p className="d-inline-block">Komentarz:<br/> <small>{comment}</small></p><br/>
                                <small className="text-muted">{date}</small>
                            </Col>
                        </Row>
                    </Container>
                </Card.Title>
                <Card.Title>
                    <hr/>
                    {userData.role == '2' &&
                    <>
                        <Formik
                            initialValues={{id: id, price: ""}}
                            onSubmit = {({id, price}) => {
                                updatePricing(id, price)
                            }}
                            render={() => (
                                <Form>
                                    <Field
                                        name="price"
                                        render={({field, form, meta}) => (
                                                <Input {...field} type="number" placeholder={price}/>
                                        )}
                                    />
                                    <Button type="submit">Wstępna wycena</Button>

                                </Form>
                            )}
                        />
                    </>
                    }
                    {userData.role == '0' &&
                    <>
                        <p className="d-inline-block">Wstępna wycena: {!price ? <small>Oczekuje na wycenę</small> : price + " PLN"}</p><br/>
                    </>
                    }
                </Card.Title>
            </Card.Body>
        </Card>
    )
};

const mapStateToProps = ({ statusPricingID = null}) => ({
    statusPricingID
});

const mapDispatchToProps = dispatch => ({
    updatePricing: (id, price) =>
        dispatch(updatePricingAction(id, price))
});

export default connect(mapStateToProps, mapDispatchToProps)(ValuationItem);