import React from "react";
import {Card, Badge, Container, Row, Col, OverlayTrigger, Tooltip} from "react-bootstrap";
import Button from "../Button/Button";
import Input from "../Input/Input";
import {Field, Form, Formik} from "formik";
import {updateStatusRepair as updateStatusRepairAction} from "../../../actions";
import {connect} from "react-redux";


const UpdateRepairItem = ({status, car, model, vin, updateStatusRepair}) => (
    <Card className="m-3">
        <Card.Body>
            <Card.Title>
                <Container fluid>
                    <Row>
                        <Col xs={12} md={7}>
                            <h4 className="d-inline-block"><Badge className="mr-1"
                                                                  variant={status !== 'ukonczono' ? "success" : "secondary"}>{status !== 'ukonczono' ? "W realizacji" : "Zakończone"}</Badge>
                            </h4>
                            <p className="d-inline-block">Samochód: {car + ' ' + model}</p><br/>
                            <p className="d-inline-block">Numer VIN: {vin}</p><br/>
                        </Col>
                        <Col xs={12} md={5}>
                            <p className="d-block">
                                <div className="d-block">
                                    <p className="d-inline-block">Numer śledzenia: <b>{vin}</b></p><br/>
                                    <p className="d-inline-block">Aktualny status: {status}</p><br/>
                                </div>
                            </p>
                        </Col>
                    </Row>
                    <hr/>
                    {status !== 'ukonczono' &&
                    <Row>
                        <Col xs={12} md={7}>
                            <Formik
                                initialValues={{vin: vin, status: '', flag: '2'}}
                                onSubmit={({flag, vin, status}) => {
                                    updateStatusRepair(flag, vin, status)
                                }}
                                render={() => (
                                    <Form>
                                        <Field
                                            name="status"
                                            render={({field, form, meta}) => (
                                                <div>
                                                    <Input className="w-100 m-2" {...field}
                                                           placeholder="Komentarz statusu" type="text" required/>
                                                    <Button type={"submit"} width={"50%"}
                                                            className={"mt-3"}>Aktualizuj</Button>
                                                </div>
                                            )}
                                        />

                                    </Form>
                                )}
                            />
                        </Col>
                        <Col xs={12} md={5}>
                            <Formik
                                initialValues={{vin: vin, status: 'Naprawa zakończona', flag: '1'}}
                                onSubmit={({flag, vin, status}) => {
                                    updateStatusRepair(flag, vin, status)
                                }}
                                render={() => (
                                    <Form>
                                        <Field
                                            name="description"
                                            render={({field, form, meta}) => (
                                                <div>
                                                    <Button type={"submit"} width={"50%"}
                                                            className={"w-100"}>Zakończ</Button>
                                                </div>
                                            )}
                                        />

                                    </Form>
                                )}
                            />
                        </Col>
                    </Row>
                    }

                </Container>
            </Card.Title>
        </Card.Body>
    </Card>
);

const mapStateToProps = ({errID = null, statusEditID = null}) => ({
    statusEditID,
    errID
});

const mapDispatchToProps = dispatch => ({
    updateStatusRepair: (flag, vin, status) =>
        dispatch(updateStatusRepairAction(flag, vin, status))
});

export default connect(mapStateToProps, mapDispatchToProps)(UpdateRepairItem);
