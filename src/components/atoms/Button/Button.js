import styled, {css} from 'styled-components';

const Button = styled.button`
  padding: 0;
  margin: 5px;
  background-color: ${({theme}) => theme.primary};
  color: #FFF !important;
  height: 47px;
  width: ${({width}) => width || '160px'};
  border: none;
  border-radius: 50px;
  font-weight: ${({theme}) => theme.bold};
  font-size: 16px;
  text-decoration: none;
  
  :hover{
     background-color: ${({theme}) => theme.primaryDarker};
     transition: 0.4s;
  }
  
  ${({secondary}) =>
    secondary &&
    css`
      background-color: ${({theme}) => theme.secondary};
      :hover{
         background-color: ${({theme}) => theme.secondaryDarker};
         transition: 0.4s;
      }
    `}
  
  ${({accept}) =>
    accept &&
    css`
      background-color: ${({theme}) => theme.accept};
      :hover{
         background-color: ${({theme}) => theme.acceptDarker};
         transition: 0.4s;
      }
    `}
 
`;

export default Button;


