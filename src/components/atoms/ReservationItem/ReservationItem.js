import React from "react";
import {Card, Badge, Container, Row, Col} from "react-bootstrap";
import Button from "../Button/Button";
import {Field, Form, Formik} from "formik";
import Input from "../Input/Input";
import {updateReservation as updateReservationAction} from "../../../actions";
import {connect} from "react-redux";


const ReservationItem = ({status, description, id, date, role, updateReservation}) => {

    var variant;
    var monit;

    console.log(role)

    switch (status) {
        case '2':
            variant = "success"
            monit = "Zaakceptowane"
            break;
        case '1':
            variant = "warning"
            monit = "Oczekujące"
            break;
        case '3':
            variant = "secondary"
            monit = "Odrzucone"
            break;
    }

    return (
        <Card className="m-3">
            <Card.Body>
                <Card.Title>
                    <Container fluid>
                        <Row>
                            <Col xs={12} md={7}>
                                <h4 className="d-inline-block"><Badge className="mr-1"
                                                                      variant={variant}>{monit}</Badge>
                                </h4>
                                <p className="d-inline-block">ID rezerwacji: {id}</p><br/>
                                <p className="d-inline-block">Komentarz:<br/> <small>{description}</small></p><br/>
                                <p className="d-inline-block">Termin: <b>{date}</b></p><br/>
                            </Col>
                            <Col xs={12} md={5}>
                                {role == '2' &&
                                <p className="d-block">
                                    {status === '1' ?
                                        <>
                                            <Formik
                                                initialValues={{id: id, status: "2"}}
                                                onSubmit={({id, status}) => {
                                                    updateReservation(id, status)
                                                }}
                                                render={() => (
                                                    <Form>
                                                        <Button type="submit" accept width={"100%"}>Akceptuj</Button>
                                                    </Form>
                                                )}
                                            />
                                            <Formik
                                                initialValues={{id: id, status: "3"}}
                                                onSubmit={({id, status}) => {
                                                    updateReservation(id, status)
                                                }}
                                                render={() => (
                                                    <Form>
                                                        <Button type="submit" width={"100%"}>Odrzuć</Button>
                                                    </Form>
                                                )}
                                            />
                                        </>
                                        : ''
                                    }

                                </p>
                                }

                            </Col>
                        </Row>
                    </Container>
                </Card.Title>
            </Card.Body>
        </Card>
    )
};

const mapStateToProps = ({errID = null}) => ({
    errID
});

const mapDispatchToProps = dispatch => ({
    updateReservation: (id, status) =>
        dispatch(updateReservationAction(id, status))
});

export default connect(mapStateToProps, mapDispatchToProps)(ReservationItem);
