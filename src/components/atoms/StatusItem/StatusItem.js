import React from "react";
import {Card, Badge, Container, Row, Col} from "react-bootstrap";
import Button from "../Button/Button";
import Input from "../Input/Input";


const StatusItem = ({id, comment, date, price}) => {

    const userData = JSON.parse(sessionStorage.getItem('userData'))

    return (
        <Card className="m-3">
            <Card.Body>
                <Card.Title>
                    <Container fluid>
                        <Row>
                            <Col xs={12} md={8}>
                                <p className="d-inline-block">Komentarz:<br/> <small>{comment}</small></p><br/>
                                <small className="text-muted">{date}</small>
                            </Col>
                            <Col xs={12} md={4} className="text-center">
                                <p className="d-inline-block">Załącznik</p>
                                <Button width={"100%"}>Pobierz</Button>
                            </Col>
                        </Row>
                    </Container>
                </Card.Title>
            </Card.Body>
        </Card>
    )
};

export default StatusItem;