import React from "react";
import styled from "styled-components";

import {Nav} from 'react-bootstrap'

const StyleWrapper = styled.div`
  position: sticky;
  top: 0;
  z-index: 1000;
  text-align: center;
  vertical-align: middle;
  background-color: ${({theme}) => theme.primary};
  border-bottom: 3px solid ${({theme}) => theme.secondaryDarker};
  ;
`
const Item = styled.div`
  padding: 5px;
  display: inline-block;
  :hover{
     background-color: ${({theme}) => theme.secondary};
     transition: 0.4s;
  }
`
const Span = styled.span`
  color: #fff;
`

const Menu = () => (
    <StyleWrapper>
        <Nav className="justify-content-center" activeKey="/home">
            <Nav.Item>
                <Item><Nav.Link href="#home"><Span>Stona główna</Span></Nav.Link></Item>
            </Nav.Item>
            <Nav.Item>
                <Item><Nav.Link href="#oferta"><Span>Oferta</Span></Nav.Link></Item>
            </Nav.Item>
            <Nav.Item>
                <Item><Nav.Link href="#rezerwacja"><Span>Rezerwacja terminu</Span></Nav.Link></Item>
            </Nav.Item>
            <Nav.Item>
                <Item><Nav.Link href="#wycena"><Span>Wycena naprawy</Span></Nav.Link></Item>
            </Nav.Item>
            <Nav.Item>
                <Item><Nav.Link href="#kontakt"><Span>Kontakt</Span></Nav.Link></Item>
            </Nav.Item>
        </Nav>

    </StyleWrapper>
);

export default Menu;
