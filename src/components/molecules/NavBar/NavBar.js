import React, {useState} from "react";
import styled from "styled-components";
import Button from "../../atoms/Button/Button";
import Input from "../../atoms/Input/Input";
import logo from '../../../assets/infix.png';

import Menu from "components/molecules/Menu/Menu";


import {Navbar, Nav, Modal, Alert} from 'react-bootstrap';
import {Redirect} from "react-router-dom";


const Logo = styled.img`
  display: inline;
  width:150px;
  vertical-align: middle;
  float: left;
  margin-left: 40px;
`

const logout = () => {
    sessionStorage.removeItem('userData');
    window.location.reload(false);
    return <Redirect to={"/"}/>
}

const guestLink = (
    <>
        <a href="/login"><Button>Zaloguj</Button></a>
        <a href="/register"><Button secondary>Rejestracja</Button></a>
    </>
)

const clientLink = (
    <>
        <a href="/controlPanel" className={window.location.pathname == '/controlPanel' ? 'd-none' : 'd-block mr-3'}><Button width={"100%"} className={"pl-3 pr-3"}>Panel
            Użytkownika</Button></a>
        <a href="/" className={window.location.pathname != '/controlPanel' ? 'd-none' : 'd-block mr-3'}><Button width={"100%"} className={"pl-3 pr-3"}>Strona
            Główna</Button></a>
        <Button secondary onClick={logout}>Wyloguj</Button>
    </>
)

const workerLink = (
    <>
        <a href="/controlPanel" className={window.location.pathname == '/controlPanel' ? 'd-none' : 'd-block mr-3'}><Button width={"100%"} className={"pl-3 pr-3"}>Panel
            Pracownika</Button></a>
        <a href="/" className={window.location.pathname != '/controlPanel' ? 'd-none' : 'd-block mr-3'}><Button width={"100%"} className={"pl-3 pr-3"}>Strona
            Główna</Button></a>
        <Button secondary onClick={logout}>Wyloguj</Button>
    </>
)

const adminLink = (
    <>
        <a href="/controlPanel" className={window.location.pathname == '/controlPanel' ? 'd-none' : 'd-block mr-3'}><Button width={"100%"} className={"pl-3 pr-3"}>Panel
            Administratora</Button></a>
        <a href="/" className={window.location.pathname != '/controlPanel' ? 'd-none' : 'd-block mr-3'}><Button width={"100%"} className={"pl-3 pr-3"}>Strona
            Główna</Button></a>
        <Button secondary onClick={logout}>Wyloguj</Button>
    </>
)

const checkRole = () => {
    var userLink;
    const userData = JSON.parse(sessionStorage.getItem('userData'))
    if (userData) {
        switch (userData.role) {
            case '0': {
                userLink = clientLink;
            }
                break;
            case '1': {
                userLink = workerLink;
            }
                break;
            case '2': {
                userLink = adminLink;
            }
                break;
            default: {
                userLink = guestLink;
            }
                break;
        }
    } else {
        userLink = guestLink;
    }

    return userLink
}

const NavBar = () => {
    return (
        <div id="home">
            <Navbar bg="light" expand="lg">
                <a href="/"><Logo src={logo} alt="Logo"/></a>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="m-auto">

                    </Nav>
                    {checkRole()}
                </Navbar.Collapse>
            </Navbar>
        </div>
    )
};

export default NavBar;
