import React from "react";

import ValuationItem from "../../atoms/ValuationItem/ValuationItem";
import axios from "axios";


class Valuation extends React.Component {
    constructor(props) {
        super();
        this.state =
            {
                itemlist: ''
            };

    }

    componentDidMount() {
        var userData = JSON.parse(sessionStorage.getItem('userData'));

        if (userData.role === '2') {
            axios.get('http://localhost:8080/show-pricing')
                .then(response => {
                    var valuationData = response.data.reverse().map((data) =>
                            <ValuationItem id={data.idpricing} comment={data.description} price={data.price}/>
                    );
                    this.setState({itemlist: valuationData});
                });
        }
        else{
            axios.get('http://localhost:8080/show-pricing/'+userData.user)
                .then(response => {
                    var valuationData = response.data.reverse().map((data) =>
                        <ValuationItem id={data.idpricing} comment={data.description} price={data.price}/>
                    );
                    this.setState({itemlist: valuationData});
                });
        }




    }

    render() {
        return (
            <>
                {this.state.itemlist}
            </>
        )
    }

};

export default Valuation;