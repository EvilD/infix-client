import React from "react";

import HistoryItem from "../../atoms/HistoryItem/HistoryItem";
import axios from "axios";

class History extends React.Component {
    constructor(props) {
        super();
        this.state =
            {
                itemlist: ''
            };

    }


    componentDidMount() {
        var userData = JSON.parse(sessionStorage.getItem('userData'));

        if (userData.role === '2') {
            axios.get('http://localhost:8080/show-allrepair')
                .then(response => {
                    var repairData = response.data.reverse().map((data) =>
                        <HistoryItem searchID={data.vin} status={data.dateFinish} car={data.marka}
                                     model={data.model}  vin={data.vin} status={data.status}/>
                    );
                    this.setState({itemlist: repairData});
                });
        }
        else{
            axios.get('http://localhost:8080/show-history/'+userData.user)
                .then(response => {
                    var repairData = response.data.reverse().map((data) =>
                        <HistoryItem searchID={data.vin} status={data.dateFinish} car={data.marka}
                                     model={data.model}  vin={data.vin} status={data.status}/>
                    );
                    this.setState({itemlist: repairData});
                });
        }




    }


    render() {
        return (
            <>
                {this.state.itemlist}
            </>
        )
    }

};

export default History;