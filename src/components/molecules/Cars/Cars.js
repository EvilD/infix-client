import React from "react";

import CarItem from "../../atoms/CarItem/CarItem";
import {Container, Row, Col, Alert} from "react-bootstrap";
import Button from "../../atoms/Button/Button";
import {Field, Form, Formik} from "formik";

import {addCar as addCarAction} from "../../../actions/";
import {connect} from "react-redux";
import axios from "axios";

class Cars extends React.Component {
    constructor(props) {
        super();
        this.state =
            {
                itemlist: '',
                login: '',
                userRole: '',
            };

    }

    componentDidMount() {

        var userData = JSON.parse(sessionStorage.getItem('userData'));
        this.setState({login: userData.user});
        this.setState({userRole: userData.role});


        if (userData.role === '2') {
            this.setState({login: ''});

            axios.get('http://localhost:8080/show-allcars')
                .then(response => {
                    var CarData = response.data.reverse().map((car) =>
                        <CarItem searchID={car.vin}
                                 car={car.marka}
                                 yearOf={car.year_of}
                                 engineCapacity={car.engine_capacity}
                                 name={car.name}
                                 surname={car.surname}
                                 model={car.model} vin={car.vin}
                                 email={car.email} phone={car.tele_no} user={this.state.userRole}/>
                    );
                    this.setState({itemlist: CarData});
                });
        } else {
            axios.get('http://localhost:8080/get-car/' + userData.user)
                .then(response => {
                    var CarData = response.data.reverse().map((car) =>
                        <CarItem searchID={car.vin}
                                 car={car.marka}
                                 yearOf={car.yearOf}
                                 engineCapacity={car.engineCapacity}
                                 model={car.model} vin={car.vin}
                                 user={this.state.userRole}/>
                    );
                    this.setState({itemlist: CarData});
                });
        }


    }


    render() {

        var userData = JSON.parse(sessionStorage.getItem('userData'));

        if (this.props.statusCarID === "CAR_SUCCESS") {
            alert = <Alert variant="success">Dodano samochód!</Alert>
        } else if (this.props.statusCarID === "CAR_FAILURE") {
            alert = <Alert variant="danger">Niepoprawne dane!</Alert>
        }

        return (
            <>
                <>
                    {alert}
                    <Formik
                        initialValues={{
                            vin: '',
                            yearOf: '',
                            marka: '',
                            model: '',
                            engineCapacity: '',
                            login: userData.user
                        }}
                        onSubmit={({vin, yearOf, marka, model, engineCapacity, login}) => {
                            this.props.addCar(vin, yearOf, marka, model, engineCapacity, login)
                        }}
                        render={() => (
                            <Form className={"w-100"}>
                                <Container>
                                    <Row>
                                        <Col>
                                            <Field
                                                name="marka"
                                                render={({field, form, meta}) => (
                                                    <div>
                                                        <small>Marka</small>
                                                        <input className={"form-control"} type="text" {...field}
                                                               placeholder="Marka"/>
                                                    </div>
                                                )}
                                            />
                                            <Field
                                                name="model"
                                                render={({field, form, meta}) => (
                                                    <div>
                                                        <small>Marka</small>
                                                        <input className={"form-control"} type="text" {...field}
                                                               placeholder="Model"/>
                                                    </div>
                                                )}
                                            />
                                            <Field
                                                name="engineCapacity"
                                                render={({field, form, meta}) => (
                                                    <div>
                                                        <small>Pojemność silnnika (cm3)</small>
                                                        <input className={"form-control"} type="text" {...field}
                                                               placeholder="Pojemność silnnika"/>
                                                    </div>
                                                )}
                                            />
                                        </Col>
                                        <Col>
                                            <Field
                                                name="vin"
                                                render={({field, form, meta}) => (
                                                    <div>
                                                        <small>Numer Vin</small>
                                                        <input className={"form-control"} type="text" {...field}
                                                               placeholder="Numer Vin"/>
                                                    </div>
                                                )}
                                            />
                                            <Field
                                                name="yearOf"
                                                render={({field, form, meta}) => (
                                                    <div>
                                                        <small>Rok Produkcji</small>
                                                        <input className={"form-control"} type="text" {...field}
                                                               placeholder="Rok produkcji"/>
                                                    </div>
                                                )}
                                            />
                                            {this.state.userRole === '2' &&
                                            <Field
                                                name="login"
                                                render={({field, form, meta}) => (
                                                    <div>
                                                        <small>Login Użytkownika</small>
                                                        <input className={"form-control"} type="text" {...field}
                                                               placeholder="Login Użytkownika"/>
                                                    </div>
                                                )}
                                            />
                                            }
                                        </Col>
                                    </Row>
                                </Container>
                                <hr/>
                                <div className={"text-center"}>
                                    <Button type={"submit"} className={"mt-3"} width={"50%"}>Dodaj</Button>
                                </div>
                            </Form>
                        )}
                    />
                    < hr/>
                </>
                {this.state.itemlist}
            </>
        )
    }
};

const mapStateToProps = ({errID = null, statusCarID = null}) => ({
    statusCarID,
    errID
});

const mapDispatchToProps = dispatch => ({
    addCar: (vin, yearOf, marka, model, engineCapacity, login) =>
        dispatch(addCarAction(vin, yearOf, marka, model, engineCapacity, login))
});

var addCar


export default connect(mapStateToProps, mapDispatchToProps)(Cars);