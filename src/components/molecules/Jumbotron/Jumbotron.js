import React from "react";

import img from "../../../../src/assets/Carousel1.jpg";
import Input from "../../atoms/Input/Input";

import {Jumbotron, Container, Nav, Alert} from 'react-bootstrap';
import Button from "../../atoms/Button/Button";
import {Link} from "react-router-dom";
import axios from "axios";
import HistoryItem from "../../atoms/HistoryItem/HistoryItem";

class JumbotronView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            inputValue: '',
            alert: ''
        };

    }

    render() {
        return (
            <Jumbotron fluid className="m-0"
                       style={{background: `url(${img})`, backgroundSize: 'cover', backgroundAttachment: 'fixed'}}>
                <Container className="m-auto">
                    <div className="text-white">
                        {this.props.search ?
                            <>
                                <h2 className="d-block text-center mb-4 display-4">Sprawdź status naprawy</h2>
                                {this.state.alert}
                                <Input className="d-block m-auto" width="50%" search
                                       placeholder={'Podaj numer naprawy'}
                                       value={this.state.inputValue} onChange={evt => this.updateInputValue(evt)}/>
                                <div className="d-block m-auto w-50">
                                    <Button className="w-100 mt-3" type="Submit" onClick={() => this.getStatus(this.state.inputValue)}>Szukaj</Button>
                                </div>

                            </>
                            :
                            <div className={"text-center"}>
                                <h1 className={"display-4"}>"U nas naprawisz raz, a dobrze"</h1>
                                <h3 >Najlepszy serwis samochodowy w okolicy!</h3>
                            </div>
                        }
                    </div>
                </Container>
            </Jumbotron>
        )
    }

    updateInputValue(evt) {
        this.setState({
            inputValue: evt.target.value
        });
    }

    getStatus(vin) {
        axios.get('http://localhost:8080/search/'+vin)
            .then(response => {
                if(response.data[0]){
                    this.setState({alert: <Alert variant="success">{response.data[0].status}</Alert>});
                }
                else{
                    this.setState({alert: <Alert variant="danger">Nie znaleziono naprawy!</Alert>});
                }
            });
    }

};


export default JumbotronView;
