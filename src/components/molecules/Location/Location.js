import React from "react";

import {Container, Row, Col} from 'react-bootstrap'
import styled from "styled-components";

const StyleWrapper = styled.div`
  background-color: #f8f9fa;
  padding: 25px;
  border-top: 1px solid rgba(34,34,34,0.17);
`

const LocationView = () => (
    <StyleWrapper>
        <Container>
            <Row>
                <Col>
                    <h4>
                        Zadzwoń i umów się na wizytę w serwisie lub uzyskaj potrzebne informacje!
                    </h4>
                    <p>
                        <b>InFix</b><br/>
                        Wawel 5<br/>
                        31-001 Kraków<br/>
                        NIP: 123-456-78-90
                    </p>
                    <hr/>
                    <p>GODZINY OTWARCIA:<br/>
                        Pon-pt: 8:00 - 18:00<br/>
                        Sob: 8:00 - 13:00<br/>
                        <br/>
                        tel. 1234567896
                    </p>
                </Col>
                <Col xs={12} md={6}>
                    <div id="map-container-google">
                        <iframe src="https://maps.google.com/maps?q=Cracow&t=&z=13&ie=UTF8&iwloc=&output=embed"
                                frameBorder="0" className="w-100" height="350"></iframe>
                    </div>
                </Col>
            </Row>
        </Container>
    </StyleWrapper>
);

export default LocationView;
