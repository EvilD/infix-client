import React from "react";
import {Alert, Col, Container, OverlayTrigger, Row, Tooltip} from "react-bootstrap";
import styled, {createGlobalStyle, css} from "styled-components";
import Input from "../../atoms/Input/Input";
import Button from "../../atoms/Button/Button";
import {Field, Form, Formik} from "formik";
import {addPricing as addPricingAction} from "../../../actions";
import {connect} from "react-redux";

const ContentWrapper = styled.div`
  background-color: #f8f9fa;
  padding: 30px 0;
`

class ValuationForm extends React.Component {

    render() {
        const userData = JSON.parse(sessionStorage.getItem('userData'))
        let userID

        if(userData){
            userID = userData.id
        }


        if (this.props.statusAddPricingID === "ADD_PRICING_SUCCESS") {
            alert = <Alert variant="success">Przesłano wiadomość!</Alert>
        } else if (this.props.statusAddPricingID === "ADD_PRICING_FAILURE") {
            alert = <Alert variant="danger">Niepoprawne dane!</Alert>
        }

        return (
            <ContentWrapper id={"wycena"} className={"pt-5"}>
                <Container>
                    <h1 className={"mt-4 text-center"}>Wycena</h1>
                    <div className={"text-center mb-5"}>
                        {alert}
                        <Formik
                            initialValues={{id: userID, description: ''}}
                            onSubmit={({id, description}) => {
                                this.props.addPricing(id, description)
                            }}
                            render={() => (
                                <Form>
                                    <Field
                                        name="description"
                                        render={({field, form, meta}) => (
                                            <div>
                                                <Input className="w-50 m-2" {...field}
                                                       placeholder="Opis usterki" type="text" required/>
                                            </div>
                                        )}
                                    />
                                    {!userData ?
                                        <OverlayTrigger
                                            overlay={<Tooltip id="tooltip-disabled">Musisz się zalogować!</Tooltip>}>

                                <span>
                                    <Button style={{pointerEvents: 'none'}} width={"50%"} className={"mt-3"}
                                            disabled={!userData ? "disabled" : ''}
                                            secondary={!userData ? "secondary" : ''}>Wyślij prośbę o wycenę</Button>
                                </span>
                                        </OverlayTrigger> :
                                        <Button width={"50%"} className={"mt-3"}
                                                type="submit">Wyślij prośbę o wycenę</Button>
                                    }
                                </Form>
                            )}
                        />
                        <br/>
                    </div>
                </Container>
            </ContentWrapper>
        )
    }
};

const mapStateToProps = ({statusAddPricingID = null}) => ({
    statusAddPricingID
});

const mapDispatchToProps = dispatch => ({
    addPricing: (id, description) =>
        dispatch(addPricingAction(id, description))
});

export default connect(mapStateToProps, mapDispatchToProps)(ValuationForm);
