import React from "react";
import {Alert, Container, OverlayTrigger, Tooltip} from "react-bootstrap";
import styled, {createGlobalStyle, css} from "styled-components";
import Input from "../../atoms/Input/Input";

import "react-datepicker/dist/react-datepicker.css";

import "./ReservationForm.css";
import Button from "../../atoms/Button/Button";
import {Field, Form, Formik} from "formik";
import {addReservation as addReservationAction} from "../../../actions";
import {connect} from "react-redux";

const ContentWrapper = styled.div`
  background-color: #f8f9fa;
  padding: 30px 0;
`

class ReservationForm extends React.Component {
    constructor(props) {
        super();
        this.state =
            {
                alert: ''
            };

    }


    render() {
        const userData = JSON.parse(sessionStorage.getItem('userData'))
        let user
        let userID

        if(userData){
            user = userData.user
            userID = userData.id
        }

        if (this.props.statusAddReservationID === "ADD_RESERVATION_SUCCESS") {
            alert = <Alert variant="success">Przesłano wiadomość!</Alert>
        } else if (this.props.statusAddReservationID === "ADD_RESERVATION_FAILURE") {
            alert = <Alert variant="danger">Niepoprawne dane!</Alert>
        }

        return (
            <ContentWrapper id={"rezerwacja"} className={"pt-5"}>
                <Container>
                    <h1 className={"mt-4 text-center"}>Rezerwacja</h1>
                    <div className={"text-center mb-5"}>
                        {alert}
                        <Formik
                            initialValues={{idUser: userID, login: user, dateStart: '', description: '', vin: ''}}
                            onSubmit={({idUser, login, dateStart,  description, vin}) => {
                                this.props.addReservation(idUser, login, dateStart,  description, vin)
                            }}
                            render={() => (
                                <Form>
                                    <Field
                                        name="dateStart"
                                        render={({field, form, meta}) => (
                                            <div>
                                                <Input className="w-50 m-2" {...field}
                                                    pattern={"[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])"}
                                                       placeholder="RRRR-MM-DD" type="text" required/>
                                            </div>
                                        )}
                                    />
                                    <Field
                                        name="description"
                                        render={({field, form, meta}) => (
                                            <div>
                                                <Input className="w-50 m-2" {...field}
                                                       placeholder="Opis usterki" type="text" required/>
                                            </div>
                                        )}
                                    />
                                    <Field
                                        name="vin"
                                        render={({field, form, meta}) => (
                                            <div>
                                                <Input className="w-50 m-2" {...field}
                                                       placeholder="Podaj numer VIN" type="text" required/>
                                            </div>
                                        )}
                                    />
                                    {!userData ?
                                        <OverlayTrigger
                                            overlay={<Tooltip id="tooltip-disabled">Musisz się zalogować!</Tooltip>}>

                                <span>
                                    <Button style={{pointerEvents: 'none'}} width={"50%"} className={"mt-3"}
                                            disabled={!userData ? "disabled" : ''}
                                            secondary={!userData ? "secondary" : ''}>Wyślij prośbę o wycenę</Button>
                                </span>
                                        </OverlayTrigger> :
                                        <Button width={"50%"} className={"mt-3"}
                                                type="submit">Wyślij prośbę o
                                            rezerwację</Button>
                                    }
                                </Form>
                            )}
                        />
                    </div>
                </Container>
            </ContentWrapper>
        )
    }
};


const mapStateToProps = ({statusAddReservationID = null}) => ({
    statusAddReservationID
});

const mapDispatchToProps = dispatch => ({
    addReservation: (idUser, login, dateStart,  description, vin) =>
        dispatch(addReservationAction(idUser, login, dateStart,  description, vin))
});

export default connect(mapStateToProps, mapDispatchToProps)(ReservationForm);

