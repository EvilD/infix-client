import React from "react";

import UpdateRepairItem from "../../atoms/UpdateRepairItem/UpdateRepairItem";
import axios from "axios";
import HistoryItem from "../../atoms/HistoryItem/HistoryItem";


class UpdateRepair extends React.Component {
    constructor(props) {
        super();
        this.state =
            {
                itemlist: ''
            };

    }


    componentDidMount() {
        var userData = JSON.parse(sessionStorage.getItem('userData'));


            axios.get('http://localhost:8080/show-allrepair/'+userData.user)
                .then(response => {
                    var repairData = response.data.reverse().map((data) =>
                        <UpdateRepairItem status={data.status} car={data.marka}
                                 model={data.model}  vin={data.vin}/>
                    );
                    this.setState({itemlist: repairData});
                });



    }

    // const numbers = [1, 2, 3, 4, 5];
    // const listItems = numbers.map((number) =>
    //     <UpdateRepairItem id={number} searchID={"dfda4e4dfe3d4cce84"} status={"true"} date={"20.06.2020 21:37"} car={"Fiat"}
    //              model={"Punto"} registrationNumber={"K0 MISIEK"} vin={"12345678942324"}/>
    // );


    render() {
        return (
            <>
                {this.state.itemlist}
            </>
        )
    }

};

export default UpdateRepair;