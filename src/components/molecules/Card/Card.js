import React from "react";
import styled from "styled-components";
import Button from "../../atoms/Button/Button";

const StyleWrapper = styled.div`
  box-shadow: 0 10px 30px -10px hsla(0,0%,0%,0.1);
  border-radius: 10px;
  overflow: hidden;
  ;
`

const InnerWrapper = styled.div`
  padding: 17px 30px 10px;
  background-color: ${({head, theme}) => (head ? 
    theme.primary : "white")};
`

const Card = () => (
    <StyleWrapper>
        <InnerWrapper head>
            asdasdasdasd
        </InnerWrapper>
        <InnerWrapper>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum provident sequi soluta voluptatum. Commodi, corporis delectus illo in perspiciatis reiciendis! Adipisci consequatur dolorum eos nisi repellat. Animi hic inventore quia!
        <Button secondary>Zamknij</Button>
        </InnerWrapper>
    </StyleWrapper>
);

export default Card;
