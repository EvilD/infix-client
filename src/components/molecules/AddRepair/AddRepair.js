import React from "react";


import {Container, Row, Col, Alert} from "react-bootstrap";
import Button from "../../atoms/Button/Button";

import {Field, Form, Formik} from "formik";
import {addRepair as addRepairAction} from "../../../actions/";
import {connect} from "react-redux";

const AddRepair = ({addRepair, statusRepairID, errID}) => {
    let alert;

    console.log(statusRepairID)

    if(statusRepairID === "ADD_REPAIR_SUCCESS"){
        alert = <Alert variant="success">Rozpoczęto naprawę!</Alert>
    }
    else if(statusRepairID === "ADD_REPAIR_FAILURE"){
        alert = <Alert variant="danger">Niepoprawne dane!</Alert>
    }

    return (

        <>
            {alert}
            <Formik
                initialValues={{vin: '', status: '', login: ''}}
                onSubmit={({status, vin, login}) => {
                    addRepair(status, vin, login)
                }}
                render={() => (
                    <Form className={"w-100"}>
                        <Container>
                            <Field
                                name="login"
                                render={({field, form, meta}) => (
                                    <div>
                                        <small>Login pracownika</small>
                                        <input className={"form-control"} type="text" {...field}
                                               placeholder="Login pracownika"/>
                                    </div>
                                )}
                            />
                            <Field
                                name="vin"
                                render={({field, form, meta}) => (
                                    <div>
                                        <small>Numer Vin</small>
                                        <input className={"form-control"} type="text" {...field}
                                               placeholder="Numer Vin"/>
                                    </div>
                                )}
                            />
                            {/*<Field*/}
                            {/*    name="status"*/}
                            {/*    render={({field, form, meta}) => (*/}
                            {/*        <div>*/}
                            {/*            <small>Komentarz</small>*/}
                            {/*            <input className={"form-control"} type="text" {...field}*/}
                            {/*                   placeholder="Komentarz"/>*/}
                            {/*        </div>*/}
                            {/*    )}*/}
                            {/*/>*/}
                        </Container>
                        <div className={"text-center"}>
                            <Button type={"submit"} className={"mt-3"} width={"50%"}>Dodaj</Button>
                        </div>
                    </Form>
                )}
            />
            <hr/>
        </>
    )
};


const mapStateToProps = ({errID = null, statusRepairID = null}) => ({
    statusRepairID,
    errID
});

const mapDispatchToProps = dispatch => ({
    addRepair: (status, vin, login) =>
        dispatch(addRepairAction(status, vin, login))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddRepair);