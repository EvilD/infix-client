import React from "react";
import styled from "styled-components";
import {Container, Row, Col, Image} from 'react-bootstrap'
import logo from '../../../assets/infix.png';

const StyleWrapper = styled.div`
  position: sticky;
  top: 0;
  z-index: 1000;
  vertical-align: middle;
  color: white;
  background-color: #262626;
  ;
`


const Footer = () => (
    <StyleWrapper id={"kontakt"}>
        <Container>
            <Row className="p-4">
                <Col>
                    <Image src={logo} className="w-50"/>
                    <p className="pt-3">
                        <b>InFix</b><br/>
                        Wawel 5<br/>
                        31-001 Kraków<br/>
                        NIP: 123-456-78-90
                    </p>
                </Col>
                <Col>
                </Col>
                <Col>
                    <h3>Polityka cookies</h3>
                    <p>Strona korzysta z plików cookies w celu realizacji usług. Możesz określić warunki przechowywania
                        lub dostępu do plików cookies w swojej przeglądarce.</p>
                </Col>
            </Row>
        </Container>
    </StyleWrapper>
);

export default Footer;
