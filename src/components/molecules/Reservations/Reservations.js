import React from "react";
import ReservationItem from "../../atoms/ReservationItem/ReservationItem";
import HistoryItem from "../../atoms/HistoryItem/HistoryItem";
import axios from "axios";
import CarItem from "../../atoms/CarItem/CarItem";

class Reservations extends React.Component {
    constructor(props) {
        super();
        this.state =
            {
                itemlist: ''
            };

    }

    componentDidMount() {
        var userData = JSON.parse(sessionStorage.getItem('userData'));

        if (userData.role === '2') {
            axios.get('http://localhost:8080/show-pending')
                .then(response => {
                    var CarData = response.data.reverse().map((data) =>
                        <ReservationItem status={data.status} date={data.dateFinish}
                                         description={data.description}
                                         id={data.idReservation}
                                         role={userData.role}/>
                    );
                    this.setState({itemlist: CarData});
                });
        } else {
            axios.get('http://localhost:8080/show-history/'+userData.user)
                .then(response => {
                    var CarData = response.data.reverse().map((data) =>
                        <ReservationItem status={data.status} date={data.date_start}
                                         description={data.description}
                                         id={data.idreservation}
                                         role={userData.role}/>
                    );
                    this.setState({itemlist: CarData});
                });
        }


    }


    render() {
        return (
            <>
                {this.state.itemlist}
            </>
        )
    }

};

export default Reservations;