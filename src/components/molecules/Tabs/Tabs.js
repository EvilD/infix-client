import React from "react";
import styled from "styled-components";

import {Tab, Row, Col, Nav, Alert} from 'react-bootstrap'
import Button from "../../atoms/Button/Button";
import History from "../History/History";
import Valuation from "../Valuation/Valuation";
import Users from "../Users/Users";
import Reservations from "../Reservations/Reservations";
import Cars from "../Cars/Cars";
import AddRepair from "../AddRepair/AddRepair";
import UpdateRepair from "../UpdateRepair/UpdateRepair";
import {Field, Form, Formik} from "formik";
import Input from "../../atoms/Input/Input";
import {editUser as editUserAction} from "../../../actions";
import {connect} from "react-redux";
import axios from "axios";
import {Redirect} from "react-router-dom";

const ContentWrapper = styled.div`
  margin: 2%
`

const Tabs = ({editUser, statusEditID}) => {

    const userData = JSON.parse(sessionStorage.getItem('userData'))

    var alert;

    if(statusEditID === "EDIT_USER_SUCCESS"){
        alert = <Alert variant="success">Dane zmienione!</Alert>

        var login = userData.user;
        var roleID = userData.role.toString();
        var id = userData.id;

        axios.get('http://localhost:8080/edit/'+login)
            .then(response => {
                console.log(response)
                let email = response.data[0].email;
                let phoneNumber = response.data[0].telephoneNumber;

                sessionStorage.setItem('userData', JSON.stringify({
                    'user': login,
                    'id': id,
                    'role': roleID,
                    'email': email,
                    'phoneNumber': phoneNumber
                }));
            });
    }
    else if(statusEditID === "EDIT_USER_FAILURE"){
        alert = <Alert variant="danger">Niepoprawne dane!</Alert>
    }

    return (
        <ContentWrapper>
            <Tab.Container id="left-tabs-example" defaultActiveKey="history">
                <Row>
                    <Col sm={3}>
                        <Nav variant="pills" className="flex-column">
                            <Nav.Item>
                                <Nav.Link className="btn m-1" eventKey="history">Historia</Nav.Link>
                            </Nav.Item>
                            {userData.role == 2 &&
                            <Nav.Item>
                                <Nav.Link className="btn m-1" eventKey="repair">Dodaj naprawę</Nav.Link>
                            </Nav.Item>
                            }
                            {userData.role == 1 &&
                            <Nav.Item>
                                <Nav.Link className="btn m-1" eventKey="repairUpdate">Aktualizuj naprawę</Nav.Link>
                            </Nav.Item>
                            }
                            {((userData.role == 0) || (userData.role == 2)) &&
                            <Nav.Item>
                                <Nav.Link className="btn m-1"
                                          eventKey="cars">{(userData.role == 0) ? 'Moje samochody' : 'Samochody'}</Nav.Link>
                            </Nav.Item>
                            }
                            {((userData.role == 0) || (userData.role == 2)) &&
                            <Nav.Item>
                                <Nav.Link className="btn m-1" eventKey="valuation">Wyceny</Nav.Link>
                            </Nav.Item>
                            }
                            {((userData.role == 0) || (userData.role == 2)) &&
                            <Nav.Item>
                                <Nav.Link className="btn m-1" eventKey="reservations">Rezerwacje</Nav.Link>
                            </Nav.Item>
                            }
                            {userData.role == 2 &&
                            <Nav.Item>
                                <Nav.Link className="btn m-1" eventKey="workers">Lista użytkowników</Nav.Link>
                            </Nav.Item>
                            }
                            <Nav.Item>
                                <Nav.Link className="btn m-1" eventKey="edit">Edytuj Profil</Nav.Link>
                            </Nav.Item>
                        </Nav>
                    </Col>
                    <Col sm={9}>
                        <Tab.Content>
                            <Tab.Pane eventKey="repair">
                                <AddRepair/>
                            </Tab.Pane>
                            <Tab.Pane eventKey="repairUpdate">
                                <UpdateRepair/>
                            </Tab.Pane>
                            <Tab.Pane eventKey="cars">
                                <Cars user={userData.role}/>
                            </Tab.Pane>
                            <Tab.Pane eventKey="history">
                                <History/>
                            </Tab.Pane>
                            <Tab.Pane eventKey="valuation">
                                <Valuation/>
                            </Tab.Pane>
                            <Tab.Pane eventKey="reservations">
                                <Reservations role={userData.role}/>
                            </Tab.Pane>
                            <Tab.Pane eventKey="workers">
                                <Users/>
                            </Tab.Pane>
                            <Tab.Pane eventKey="edit">
                                {alert}
                                <Formik
                                    initialValues={{login: userData.user, email: userData.email, telephoneNumber: userData.phoneNumber, password: ""}}
                                    onSubmit = {({login, email, telephoneNumber, password}) => {
                                        editUser(login, email, telephoneNumber, password)
                                    }}
                                    render={() => (
                                        <Form>
                                            <Field
                                                name="login"
                                                render={({field, form, meta}) => (
                                                    <div>
                                                        <input className={"form-control mt-3"} {...field}
                                                               placeholder="Login" type="text" disabled/>
                                                    </div>
                                                )}
                                            />
                                            <Field
                                                name="email"
                                                render={({field, form, meta}) => (
                                                    <div>
                                                        <input className={"form-control mt-3"} {...field}
                                                               placeholder="Adres email" type="text"/>
                                                    </div>
                                                )}
                                            />
                                            <Field
                                                name="telephoneNumber"
                                                render={({field, form, meta}) => (
                                                    <div>
                                                        <input className={"form-control mt-3"} {...field}
                                                               placeholder="Numer Telefonu" type="number"/>
                                                    </div>
                                                )}
                                            />
                                            <Field
                                                name="password"
                                                render={({field, form, meta}) => (
                                                    <div>
                                                        <input className={"form-control mt-3"} {...field}
                                                               placeholder="Hasło" type="password"/>
                                                    </div>
                                                )}
                                            />
                                            <hr/>
                                            <Button type="submit" width={"50%"}>Zapisz</Button>
                                        </Form>
                                    )}
                                />
                            </Tab.Pane>
                        </Tab.Content>
                    </Col>
                </Row>
            </Tab.Container>
        </ContentWrapper>
    )
};

const mapStateToProps = ({ errID = null, statusEditID = null}) => ({
    statusEditID,
    errID
});

const mapDispatchToProps = dispatch => ({
    editUser: (login, email, telephoneNumber, password) =>
        dispatch(editUserAction(login, email, telephoneNumber, password))
});

export default connect(mapStateToProps, mapDispatchToProps)(Tabs);