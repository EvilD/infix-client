import React from "react";
import styled from "styled-components";

import {Container, Row, Col, Card, CardColumns} from 'react-bootstrap'

import {MdLocalCarWash} from 'react-icons/md';
import {FaCarCrash, FaCarBattery, FaCaravan} from 'react-icons/fa';

const ContentWrapper = styled.div`
text-align: center;
  background-color: #f8f9fa;
  padding: 30px 0;
`


const NavBar = () => (
    <ContentWrapper id={"oferta"} className={"pt-5"}>
        <Container>
            <h1 className="mb-4">Nasza Oferta</h1>
            <Row>
                <Col xs={12} md={3} className="text-center mb-2">
                    <Card>
                        <Card.Body>
                            <h1><FaCarCrash/></h1>
                            <Card.Title>Naprawa</Card.Title>
                            <Card.Text>
                                Zajmujemy się szerokim wachlarzem napraw mechanicznych oraz blacharko lakierniczych.
                                Mozesz sprawdzić status swojej naprawy 27/7
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={12} md={3} className="text-center mb-2">
                    <Card>
                        <Card.Body>
                            <h1><FaCarBattery/></h1>
                            <Card.Title>Serwis</Card.Title>
                            <Card.Text>
                                W naszym serwisie dokonasz przeglądu okresowego i nie tylko. Dostępna jest również
                                hamownia, dzięki której sprawdzisz osiągi swojego samochodu
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={12} md={3} className="text-center mb-2">
                    <Card>
                        <Card.Body>
                            <h1><FaCaravan/></h1>
                            <Card.Title>Akcesoria</Card.Title>
                            <Card.Text>
                                Posiadamy szeroką gamę akcesoriów samochodowych oraz środków przeznaczonych do dbania o
                                czystość pojazdu. Dostępne w naszym serwisie od ręki
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={12} md={3} className="text-center mb-2">
                    <Card>
                        <Card.Body>
                            <h1><MdLocalCarWash/></h1>
                            <Card.Title>Mycie</Card.Title>
                            <Card.Text>
                                Oferujemy profesjonalne mycie ręczne twojego pojazdu z zastosowanie najlepszych środków
                                czystości. Zajmujemy się również konserwacją wnętrz
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
        <hr/>
    </ContentWrapper>
);

export default NavBar;