import React from "react";
import UserItem from "../../atoms/UserItem/UserItem";
import axios from "axios";
import HistoryItem from "../../atoms/HistoryItem/HistoryItem";

class Users extends React.Component {

    constructor(props) {
        super();
        this.state =
            {
                itemlist: ''
            };

    }

    componentDidMount() {
        axios.get('http://localhost:8080/show-allusers')
            .then(response => {
                var repairData = response.data.reverse().map((data) =>
                    <UserItem id={data.idUser} login={data.login} email={data.email} role={data.permision} phone={data.telephoneNumber}/>
                );
                this.setState({itemlist: repairData});
            });
    }

    // const
    // numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    // const
    // listItems = numbers.map((number) =>
    //     <UserItem id={number} login={"User"} email={"asd@asd.pl"} role={"2"} phone={"123456789"}/>
    // );

    render() {

        return (
            <>
                {this.state.itemlist}
            </>
        )
    }
};

export default Users;