import axios from 'axios';

export const AUTH_REQUEST = 'AUTHENTICATE_REQUEST';
export const AUTH_SUCCESS = 'AUTHENTICATE_SUCCESS';
export const AUTH_FAILURE = 'AUTHENTICATE_FAILURE'
export const REGISTER_REQUEST = 'REGISTER_REQUEST';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAILURE = 'REGISTER_FAILURE';
export const CAR_REQUEST = 'CAR_REQUEST';
export const CAR_SUCCESS = 'CAR_SUCCESS';
export const CAR_FAILURE = 'CAR_FAILURE';
export const ADD_REPAIR_REQUEST = 'ADD_REPAIR_REQUEST';
export const ADD_REPAIR_SUCCESS = 'ADD_REPAIR_SUCCESS';
export const ADD_REPAIR_FAILURE = 'ADD_REPAIR_FAILURE';
export const EDIT_USER_REQUEST = 'EDIT_USER_REQUEST';
export const EDIT_USER_SUCCESS = 'EDIT_USER_SUCCESS';
export const EDIT_USER_FAILURE = 'EDIT_USER_FAILURE';
export const UPDATE_PRICING_REQUEST = 'UPDATE_PRICING_REQUEST';
export const UPDATE_PRICING_SUCCESS = 'UPDATE_PRICING_SUCCESS';
export const UPDATE_PRICING_FAILURE = 'UPDATE_PRICING_FAILURE';
export const ADD_PRICING_REQUEST = 'ADD_PRICING_REQUEST';
export const ADD_PRICING_SUCCESS = 'ADD_PRICING_SUCCESS';
export const ADD_PRICING_FAILURE = 'ADD_PRICING_FAILURE';
export const ADD_RESERVATION_REQUEST = 'ADD_RESERVATION_REQUEST';
export const ADD_RESERVATION_SUCCESS = 'ADD_RESERVATION_SUCCESS';
export const ADD_RESERVATION_FAILURE = 'ADD_RESERVATION_FAILURE';
export const UPDATE_RESERVATION_REQUEST = 'UPDATE_RESERVATION_REQUEST';
export const UPDATE_RESERVATION_SUCCESS = 'UPDATE_RESERVATION_SUCCESS';
export const UPDATE_RESERVATION_FAILURE = 'UPDATE_RESERVATION_FAILURE';
export const UPDATE_REPAIR_REQUEST = 'ADD_REPAIR_REQUEST';
export const UPDATE_REPAIR_SUCCESS = 'ADD_REPAIR_SUCCESS';
export const UPDATE_REPAIR_FAILURE = 'ADD_REPAIR_FAILURE';


export const authenticate = (login, password) => dispatch => {
    dispatch({type: AUTH_REQUEST});
    return axios.post('http://localhost:8080/login', {
        login,
        password
    })
        .then(payload => dispatch({type: AUTH_SUCCESS, payload}))
        .catch(err => {
            console.log(err);
            dispatch({type: AUTH_FAILURE, err})
        })
}

export const register = (name, surname, pesel, driversLicense, password, login, email, telephoneNumber) => dispatch => {
    dispatch({type: REGISTER_REQUEST});
    return axios.post('http://localhost:8080/register', {
        name,
        surname,
        pesel,
        driversLicense,
        password,
        login,
        email,
        telephoneNumber
    })
        .then(payload => dispatch({type: REGISTER_SUCCESS, payload}))
        .catch(err => {
            console.log(err);
            dispatch({type: REGISTER_FAILURE, err})
        })
}

export const addCar = (vin, yearOf, marka, model, engineCapacity, login) => dispatch => {
    dispatch({type: CAR_REQUEST});
    console.log(login)
    return axios.post('http://localhost:8080/add-car/'+login, {
        vin,
        yearOf,
        marka,
        model,
        engineCapacity
    })
        .then(payload => dispatch({type: CAR_SUCCESS, payload}))
        .catch(err => {
            console.log(err);
            dispatch({type: CAR_FAILURE, err})
        })
}

export const addRepair = (status, vin, login) => dispatch => {
    dispatch({type: ADD_REPAIR_REQUEST});
    return axios.post('http://localhost:8080/add-repair/'+login, {
        status,
        vin
    })
        .then(payload => dispatch({type: ADD_REPAIR_SUCCESS, payload}))
        .catch(err => {
            console.log(err);
            dispatch({type: ADD_REPAIR_FAILURE, err})
        })
}

export const editUser = (login, email, telephoneNumber, password) => dispatch => {
    dispatch({type: EDIT_USER_REQUEST});
    return axios.put('http://localhost:8080/edit-send', {
        login,
        email,
        telephoneNumber,
        password
    })
        .then(payload => dispatch({type: EDIT_USER_SUCCESS, payload}))
        .catch(err => {
            console.log(err);
            dispatch({type: EDIT_USER_FAILURE, err})
        })
}

export const updatePricing = (id, price) => dispatch => {
    dispatch({type: UPDATE_PRICING_REQUEST});
    return axios.put('http://localhost:8080/update-pricing/'+id, {
        price
    })
        .then(payload => dispatch({type: UPDATE_PRICING_SUCCESS, payload}))
        .catch(err => {
            console.log(err);
            dispatch({type: UPDATE_PRICING_FAILURE, err})
        })
}

export const addPricing = (idUser, description) => dispatch => {
    dispatch({type: ADD_PRICING_REQUEST});
    return axios.post('http://localhost:8080/add-pricing', {
        idUser,
        description
    })
        .then(payload => dispatch({type: ADD_PRICING_SUCCESS, payload}))
        .catch(err => {
            console.log(err);
            dispatch({type: ADD_PRICING_FAILURE, err})
        })
}

export const addReservation = (idUser, login, dateStart,  description, vin) => dispatch => {
    dispatch({type: ADD_RESERVATION_REQUEST});
    console.log(idUser, login, dateStart,  description, vin)
    return axios.post('http://localhost:8080/add', {
        idUser,
        dateStart,
        vin,
        description
    })
        .then(payload => dispatch({type: ADD_RESERVATION_SUCCESS, payload}))
        .catch(err => {
            console.log(err);
            dispatch({type: ADD_RESERVATION_FAILURE, err})
        })
}

export const updateReservation = (id, status) => dispatch => {
    dispatch({type: UPDATE_RESERVATION_REQUEST});
    return axios.put('http://localhost:8080/edit-reservation/'+id+'?status1='+status, {

    })
        .then(payload => dispatch({type: UPDATE_RESERVATION_SUCCESS, payload}))
        .catch(err => {
            console.log(err);
            dispatch({type: UPDATE_RESERVATION_FAILURE, err})
        })
}

export const updateStatusRepair = (flag, vin, status) => dispatch => {
    dispatch({type: UPDATE_REPAIR_REQUEST});
    return axios.put('http://localhost:8080/change-status/'+flag, {
        vin,
        status
    })
        .then(payload => dispatch({type: UPDATE_REPAIR_SUCCESS, payload}))
        .catch(err => {
            console.log(err);
            dispatch({type: UPDATE_REPAIR_FAILURE, err})
        })
}